﻿using System;
namespace Otus.Teaching.Pcf.Administration.Intergation
{
    public class NotifyAdminAboutPartnerManagerPromoCodeMsg
    {
        public Guid? PartnerManagerId { get; set; }
    }
}

