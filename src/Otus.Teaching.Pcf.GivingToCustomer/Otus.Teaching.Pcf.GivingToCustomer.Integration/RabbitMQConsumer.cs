﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Intergation;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration

{
    public class RabbitMQConsumer : BackgroundService
    {

        private readonly IConfiguration _configuration;
        private readonly IIntegrationEvents _integrationEvents;
        private IConnection _connection;
        private IModel _channel;
        private readonly string queue_NotifyCustomer = "GivePromoCodeToCustomer";

        public RabbitMQConsumer(IConfiguration configuration, 
                                IIntegrationEvents integrationEvents)

        {
            _configuration = configuration;
            _integrationEvents = integrationEvents;
            Initialize();
        }

        private void Initialize()
        {
            var factory = new ConnectionFactory()
            {
                UserName = "guest",
                Password = "guest",
                HostName = _configuration["RabbitMQHost"],
                Port = int.Parse(_configuration["RabbitMQPort"]),
            };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();


            _channel.ExchangeDeclare(exchange: queue_NotifyCustomer, type: ExchangeType.Fanout);

            _channel.QueueDeclare(queue: queue_NotifyCustomer,
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            _channel.QueueBind(queue: queue_NotifyCustomer,
                               exchange: queue_NotifyCustomer,
                               routingKey: "");

        }

        public override void Dispose()
        {
            if (_channel.IsOpen)
            {
                _channel.Close();
                _connection.Close();
            }

            base.Dispose();
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (sender, args) =>
            {
                var body = args.Body;
                var message = Encoding.UTF8.GetString(body.ToArray());

                _integrationEvents.GivePromoCodeToCustomer(message);
            };

            _channel.BasicConsume(queue: queue_NotifyCustomer,
                                    autoAck: true, consumer);

            return Task.CompletedTask;
        }
    }
}
